package com.sapient.client;

import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

public class DemoClient {
	private static URI getBaseUri() {
		return UriBuilder.fromUri("https://developers.zomato.com/api/v2.1/geoloca").build();
				}
	public static void main(String[] args) {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(getBaseUri());
		String resp = target.request().header("user-key", "76a8b712c288a57d5b04e67eb036b69e").accept(MediaType.TEXT_PLAIN).get(String.class);
		
		
		
	}
}
