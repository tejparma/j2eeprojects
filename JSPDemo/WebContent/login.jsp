<%@page import="com.sapient.student.bean.UserBean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<%
	String msg = "";
	UserBean ub = new UserBean();
	if (request.getAttribute("msg") != null) {
		msg = (String) request.getAttribute("msg");
	}
%>
<body>
	<center>
	<form action="LoginController" method="POST">
		Username: <input type="text" name="username" /> <br><br>
		Password: <input type="password" name="password" /> <br>	
		<input type="submit" value="Login" /><br>
		<label><%=msg%></label>
	</form>
	</center>
</body>
</html>