<%@page import="com.sapient.student.bean.StudentBean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<%
	String msg = "";
	StudentBean sb = new StudentBean();
	if (request.getAttribute("msg") != null) {
		msg = (String) request.getAttribute("msg");
	}
	if (request.getAttribute("sdetails") != null) {
		sb = (StudentBean) request.getAttribute("sdetails");
	}
%>
<body>
	<center>
		<h1>Student Results</h1>
		<form action='StudentMarksController' method='post'>
			<table>
				<tr>
					<td>Enter roll no.</td>
					<td><input type='text' name='rollno'
						value='<%=sb.getRollno()%>' /><%=msg%></td>
				</tr>
				<tr>
					<td>Name</td>
					<td><%=sb.getName()%></td>
				</tr>
				<tr>
					<td>Percentage</td>
					<td><%=sb.getPercent()%></td>
				</tr>
				<tr>
					<td colspan="2"><input type='submit' value='Submit' /></td>
				</tr>
			</table>
		</form>
	</center>
</body>
</html>