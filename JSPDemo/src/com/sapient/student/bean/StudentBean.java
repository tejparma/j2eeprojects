package com.sapient.student.bean;

public class StudentBean {
	private String id;
	private String name;
	private String rollno;
	private String percent;

	public StudentBean() {
		super();
	}

	public StudentBean(String id, String name, String rollno, String percent) {
		super();
		this.id = id;
		this.name = name;
		this.rollno = rollno;
		this.percent = percent;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRollno() {
		return rollno;
	}

	public void setRollno(String rollno) {
		this.rollno = rollno;
	}

	public String getPercent() {
		return percent;
	}

	public void setPercent(String percent) {
		this.percent = percent;
	}

	@Override
	public String toString() {
		return "StudentBean [id=" + id + ", name=" + name + ", rollno=" + rollno + ", percent=" + percent + "]";
	}

}
