package com.sapient.student.dao;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.sapient.student.bean.StudentBean;
import com.sapient.student.bean.UserBean;

public class AuthenticationService {

	public boolean registerUser(UserBean u) {
		try {
			Connection connection = DBConnection.getConnection();
			PreparedStatement st = connection.prepareStatement("INSERT INTO users VALUES (?,?,?)");
			st.setString(1, u.getUsername());
			st.setString(2, u.getPassword());
			st.setString(3, u.getRole());
			st.executeUpdate();

			return true;
		} catch (SQLException ex) {
			Logger.getLogger(AuthenticationService.class.getName()).log(Level.SEVERE, null, ex);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;

	}

	public boolean authenticate(String username, String password) {
		try {
			Connection connection = DBConnection.getConnection();

			PreparedStatement st = connection.prepareStatement("SELECT * FROM users WHERE username=? AND password = ?");
			st.setString(1, username);
			st.setString(2, password);
			ResultSet rs = st.executeQuery();

			if(rs.next()) {
				String user = rs.getString("username");
			}
			return true;
		} catch (SQLException ex) {
			Logger.getLogger(AuthenticationService.class.getName()).log(Level.SEVERE, null, ex);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public boolean usernameExists(String username) {
		try {
			Connection connection = DBConnection.getConnection();
			PreparedStatement st = connection.prepareStatement("SELECT * FROM users WHERE username=?");
			st.setString(1, username);

			ResultSet rs = st.executeQuery();
			rs.next();
			rs.getString("username");
			return true;
		} catch (SQLException ex) {
			Logger.getLogger(AuthenticationService.class.getName()).log(Level.SEVERE, null, ex);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;

	}

	public String getRoleOfUser(String username, String password) {
		String role = "";
		try {
			Connection connection = DBConnection.getConnection();
			PreparedStatement st = connection.prepareStatement("SELECT * FROM users WHERE username=? AND password = ?");
			st.setString(1, username);
			st.setString(2, password);
			ResultSet rs = st.executeQuery();

			if(rs.next()) {
				role = rs.getString("role");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return role;
	}
}
