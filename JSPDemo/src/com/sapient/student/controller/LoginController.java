package com.sapient.student.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sapient.student.bean.UserBean;
import com.sapient.student.dao.AuthenticationService;
import com.sapient.student.dao.StudentDAO;
import com.sapient.student.others.RBundle;

/**
 * Servlet implementation class LoginController
 */
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	private final AuthenticationService auth;
	
	public LoginController() {
		super();
		// TODO Auto-generated constructor stub
		auth = new AuthenticationService();
		auth.registerUser(new UserBean("demo", "demo", "admin"));
	}

	
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String role;
		if (auth.authenticate(username, password)) {
			role = auth.getRoleOfUser(username, password);
			if (role.equalsIgnoreCase("admin")) {
				RequestDispatcher rs = request.getRequestDispatcher("adminHome.jsp");
				rs.forward(request, response);
			}else {
				RequestDispatcher rs = request.getRequestDispatcher("marks.jsp");
				rs.forward(request, response);
			}
		} else {
			RequestDispatcher rs = request.getRequestDispatcher("login.jsp");
			request.setAttribute("msg", RBundle.getValues("Error3"));
			rs.forward(request, response);
		}

	}

}
