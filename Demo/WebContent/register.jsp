<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div class="container"><h1>Register Page</h1>


            <form action="${pageContext.request.contextPath}/register" method="POST">
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" id="username" name="username"/>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" />
                </div>
                <div class="form-group">
                    <label for="passwordRep">Password(again) ${requestScope.error}</label>
                    <input type="password" name="passwordRep" />
                </div>
                
                
                
                <button type="submit" class="btn btn-primary" value="Login">Register</button>
            </form>
        </div>
</body>
</html>