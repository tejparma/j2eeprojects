package com.sapient.restaurant.bean;

import java.util.Arrays;

public class RestaurantBean {
	private String id;
	private String name;
	private String location;
	private int tableSize;
	private int[] table;
	private String rating;
	public RestaurantBean() {
		super();
	}

	public RestaurantBean(String id, String name, String location, int tableSize, int[] table, String rating) {
		super();
		this.id = id;
		this.name = name;
		this.location = location;
		this.tableSize = tableSize;
		this.table = table;
		this.rating = rating;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public int[] getTables() {
		return table;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	@Override
	public String toString() {
		return "RestaurantBean [id=" + id + ", name=" + name + ", location=" + location + ", tables=" + Arrays.toString(table)
				+ ", rating=" + rating + "]";
	}
}
