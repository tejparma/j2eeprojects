package com.sapient.restaurant.dao;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnection {
static Connection con = null;
	
	public static Connection getConnection() throws Exception {
		try {
			if(con==null) {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","SYSTEM","tejasparmar"); 
			con.setAutoCommit(true);
		} 
		}catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return con;
		
	}

}
