package com.sapient.restaurant.server;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.apache.catalina.WebResource;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientResponse;

import com.sapient.restaurant.bean.RestaurantBean;
import com.sapient.restaurant.dao.RestaurantDAO;

@Path("/restaurants")
public class Restaurants {

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getRestaurants() {
		ArrayList<RestaurantBean> list = RestaurantDAO.getAllRestaurants();
		String s = "";
		for (RestaurantBean restaurantBean : list) {
			s += restaurantBean.toString() + "\n";
		}
		return s;
	}
	
	@GET
	@Path("/{name}")
	@Produces(MediaType.TEXT_PLAIN)
    public String getTables(@PathParam("name") String name)
    {
        ArrayList<RestaurantBean> list = RestaurantDAO.getAllRestaurants();
        String s = "";
        for(RestaurantBean e: list)
        {
            if(e.getName().equals(name))
            {
                s = Arrays.toString(e.getTables());
                break;
            }
        }
        return s;
    }
	
	 @GET
	    @Path("/{name}/1/{tableno}")
	    @Produces(MediaType.TEXT_PLAIN)
	    public String bookTable(@PathParam("name") String name, @PathParam("tableno") int tableno)
	    {
	        ArrayList<RestaurantBean> list = RestaurantDAO.getAllRestaurants();
	        String s = "";
	        for(RestaurantBean e: list)
	        {
	            if(e.getName().equals(name))
	            {
	                s = Arrays.toString(e.getTables()) +"\nTable Booked\n";
	                int a[] = e.getTables();
	                if(a[tableno]==0)
	                {
	                    a[tableno] = 1;
	                    s = Arrays.toString(a);
	                }
	                else
	                {
	                    s += "is already booked.";
	                }
	            }
	        }
	        return s;
	    }
	 @GET
	    @Path("/{name}/0/{tableno}")
	    @Produces(MediaType.TEXT_PLAIN)
	    public String unBookTable(@PathParam("name") String name, @PathParam("tableno") int tableno)
	    {
	        ArrayList<RestaurantBean> list = RestaurantDAO.getAllRestaurants();
	        String s = "";
	        for(RestaurantBean e: list)
	        {
	            if(e.getName().equals(name))
	            {
	       
	                s = Arrays.toString(e.getTables());
	                int a[] = e.getTables();
	                if(a[tableno]==1)
	                {
	                    a[tableno] = 0;
	                    s = Arrays.toString(a);
	                }
	                else
	                {
	                    s += "is already not booked.";
	                }
	            }
	        }
	        return s;
	    }
}
