package com.sapient.rest.server;



import java.sql.*;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sapient.rest.others.DBConnect;

@Path("/hello")
public class Student {
	
	@GET
	@Path("/getString")
	@Produces(MediaType.TEXT_PLAIN)
	public String getString() {
		return "Introduction to REST";
	}
	
	@GET
	@Path("/{param}")
	public Response getName(@PathParam("param") int a) {
		int c = a + 5;
		return Response.status(200).entity(c).build();  
	}
	
	@GET  
	  @Path("{id}/{fname}/{lname}/{dob}/{dno}/{gn}")  
	  public Response addStudent1(  
	          @PathParam("id") String id,  
	          @PathParam("fname") String fname,  
	          @PathParam("lname") String lname,
	          @PathParam("dob") String dob,
	          @PathParam("dno") String dno,
	          @PathParam("gn") String gn
	        
			  ) {  
	 
	     String msg = insert(id,fname,lname,dob,dno,gn);  
	 
	     return Response.status(200)  
	      .entity(msg)  
	      .build();  
	  } 
	
	public String insert(String ...a1)
	{
		try{
			Connection co= DBConnect.getConnection();
			PreparedStatement ps=co.prepareStatement("Insert into studs values(?,?,?,?,?,?)");
			for(int i=1;i<=a1.length;i++)
			{
				ps.setString(i, a1[i-1]);
			}
			ps.executeQuery();
			co.commit();
			return "Inserted";
		}
		catch (Exception e) {
			// TODO: handle exception
			return e.getMessage() + "LOL";
		}
	}
	
	@GET  
	  @Path("{year}/{month}/{day}")  
	  public Response getDate(  
	          @PathParam("year") int year,  
	          @PathParam("month") int month,   
	          @PathParam("day") int day) {  
	 
	     String date = year + "/" + month + "/" + day;  
	 
	     return Response.status(200)  
	      .entity("getDate is called, year/month/day : " + date)  
	      .build();  
	  } 
}
