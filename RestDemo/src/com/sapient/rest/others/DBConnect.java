package com.sapient.rest.others;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnect {
	public static Connection connection = null;
	public static Connection getConnection() throws Exception {
		if(connection==null) {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","SYSTEM","tejasparmar");
		}
		return connection;
	}
}	
